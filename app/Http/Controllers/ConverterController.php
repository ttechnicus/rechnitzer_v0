<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ConverterHelper;

class ConverterController extends Controller
{
    public function Convert(Request $request) {
        return ConverterHelper::Convert(
            $request->input('text'),
            $request->input('toVersion'),
            $request->input('fromVersion'));
    }

    public function GetConversionTable() {
        return \App\Symbol::select('v1_404', 'v2_0', 'k_v5')->get()->toArray();
    }
}
