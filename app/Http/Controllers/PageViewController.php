<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Corpus;

class PageViewController extends Controller
{
    private $defaultTextType = 'main';
    private $blocktype = [
            'h' => 'header',
            'c' => 'column',
            'b' => 'block',
            'p' => 'picture'
        ];

    /**
     * Get the list of pages
     * Pages are denoted by the folio number and letters r (for recto) and v (for verso)
     * Return value is an array of strings, each consisting the folio number on three digits,
     * and the side letter.
     */
    public function GetPageList()
    {
        return Corpus::select(DB::raw('concat(lpad(folio, 3, 0), side) as page'))
            ->distinct()
            ->get()
            ->pluck('page');
    }

    /**
     * Get the rows of a page
     * Pages are organized into text blocks.
     * Text block #0 is the "main text"
     *
     * @param string $page Page denotation which consists of the folio number on three digits, and the letter of the side.
     * @return array Returns an array of text block objects
     */
    public function GetPage($page)
    {
        // validation
        if (!preg_match("/^\d{3}[rv]$/", $page)) {
            return [];
        }

        // fetch the data
        $folio = mb_substr($page, 0, 3);
        $side = mb_substr($page, 3, 1);
        $pageRows = Corpus::where('folio', $folio)
            ->where('side', $side)
            ->get();

        return $this->ConvertPageRowsToTextBlocksObject($pageRows);
    }

    /**
     * Transform fetched page rows into a block-organized object
     */
    private function ConvertPageRowsToTextBlocksObject($corpusData) {
        $textBlocks = [];

        foreach ($corpusData as $row) {
            if (!is_null($row->blocktype) && !in_array($row->blocktype, array_keys($this->blocktype))) {
                throw new \Exception('Invalid textblock type ' . $row->blocktype);
            }

            $currentBlockType = is_null($row->blocktype)
                ? $this->defaultTextType
                : $this->blocktype[$row->blocktype];
            $currentBlockNumber = $row->blocknumber;

            if (
                count($textBlocks) == 0
                || $currentBlockType != $textBlocks[count($textBlocks) - 1]['blocktype']
                || $currentBlockNumber != $textBlocks[count($textBlocks) - 1]['blocknumber']
            ) {
                $textBlock = [
                    'blocktype' => $currentBlockType,
                    'blocknumber' => $row->blocknumber,
                    'rows' => [],
                ];
                $textBlocks[] = $textBlock;
            }

            $rowObject = [
                'text' => $row->text,
                'rownumber' => $row->rownumber,
            ];
            if (!is_null($row->alignment) && $row->alignment != '') {
                $rowObject['alignment'] = $row->alignment;
            }

            $textBlocks[count($textBlocks) - 1]['rows'][] = $rowObject;
        }

        return $textBlocks;
    }
}
