<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DictionaryController extends Controller
{
    const ROHMARKUP_ROHONC        = 'F000';
    const ROHMARKUP_ROHONC_END    = 'F001';
    const ROHMARKUP_SECTION       = 'F004';
    const ROHMARKUP_SECTION_END   = 'F005';
    const ROHMARKUP_LEMMA         = 'F006';
    const ROHMARKUP_LEMMA_END     = 'F007';
    const ROHMARKUP_META          = 'F00A';
    const ROHMARKUP_META_END      = 'F00B';
    const ROHMARKUP_UNDERLINE     = 'F002';
    const ROHMARKUP_UNDERLINE_END = 'F003';

    private $chunkTypes = [
        self::ROHMARKUP_ROHONC  => 'rohonc',
        self::ROHMARKUP_SECTION => 'section',
        self::ROHMARKUP_LEMMA   => 'lemma',
        self::ROHMARKUP_META    => 'meta',
        'default'         => null,
    ];

    const HTML_UNDERLINE     = '<u>';
    const HTML_UNDERLINE_END = '</u>';

    public function GetDictionary($language) {
        $dictionary = \App\Dictionary::select('code', 'entry')
            ->where('language', $language)
            ->get();

        foreach ($dictionary as $entry) {
            $entry->code = $this->StripCodeOfSpecialChars($entry->code);
            $entry->entry = $this->ChunkifyDictionaryEntry($entry->entry);
        }

        return $dictionary;
    }

    private function StripCodeOfSpecialChars($code)
    {
        $code = preg_replace("/[\(\)\[\]\x{".self::ROHMARKUP_UNDERLINE."}\x{".self::ROHMARKUP_UNDERLINE_END."}]/u", '', $code);
        return $code;
    }

    private function ChunkifyDictionaryEntry($text)
    {
        $text = preg_replace("/(\x{".self::ROHMARKUP_ROHONC ."}[^\x{".self::ROHMARKUP_ROHONC_END ."}]+\x{".self::ROHMARKUP_ROHONC_END ."})/u", "#$1#", $text);
        $text = preg_replace("/(\x{".self::ROHMARKUP_SECTION."}[^\x{".self::ROHMARKUP_SECTION_END."}]+\x{".self::ROHMARKUP_SECTION_END."})/u", "#$1#", $text);
        $text = preg_replace("/(\x{".self::ROHMARKUP_LEMMA  ."}[^\x{".self::ROHMARKUP_LEMMA_END  ."}]+\x{".self::ROHMARKUP_LEMMA_END  ."})/u", "#$1#", $text);
        $text = preg_replace("/(\x{".self::ROHMARKUP_META   ."}[^\x{".self::ROHMARKUP_META_END   ."}]+\x{".self::ROHMARKUP_META_END   ."})/u", "#$1#", $text);
        $text = preg_replace("/#+/u", '#', $text);
        $text = preg_replace("/(^#|#$)/u", '', $text);
        $textChunks = explode('#', $text);
        $textRichChunks = [];
        foreach ($textChunks as $chunk) {

            //Get the hexa code of the first character
            $chunkStyleChar = sprintf(
                "%04s",
                strtoupper(dechex(mb_ord(mb_substr($chunk, 0, 1)))));

            // If it is a starting markup, create rich chunk
            if (isset($this->chunkTypes[$chunkStyleChar])) {

                // Get rid of the enclosing markup characters
                $chunkText = preg_replace("/^.(.*).$/u", "$1", $chunk);

                // In case of Rohonc code chunks, replace underline markup with html underline
                if ($this->chunkTypes[$chunkStyleChar] == 'rohonc') {
                    $chunkText = preg_replace("/\x{".self::ROHMARKUP_UNDERLINE."}/u",     self::HTML_UNDERLINE,     $chunkText);
                    $chunkText = preg_replace("/\x{".self::ROHMARKUP_UNDERLINE_END."}/u", self::HTML_UNDERLINE_END, $chunkText);
                }

                // Build chunk object
                $textRichChunk = [
                    'text' => $chunkText,
                    'style' => $this->chunkTypes[$chunkStyleChar],
                ];

            // If it is a plain chunk, build the object without style
            } else {
                $textRichChunk = [
                    'text' => $chunk,
                ];
            }
            $textRichChunks[] = $textRichChunk;
        }
        return $textRichChunks;
    }
}
