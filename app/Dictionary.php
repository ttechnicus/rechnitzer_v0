<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;
}
