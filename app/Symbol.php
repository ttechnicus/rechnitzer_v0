<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Symbol extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;
}
