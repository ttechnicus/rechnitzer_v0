<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Corpus extends Model
{
    protected $table = 'corpora';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function SetLocation($location)
    {
        $validate = self::ValidateLocation($location);
        if (!$validate['isValid']) {
            throw new \Exception("Invalid location");
        }
        $matches = $validate['matches'];

        $this->folio = $matches['folio'];
        $this->side = $matches['side'];

        $this->blocktype = null;
        $this->blocknumber = null;
        if (isset($matches['header']) && $matches['header'] != '') {
            $this->blocktype = $matches['header'];
        } elseif (isset($matches['blocktype']) && $matches['blocktype'] != '') {
            $this->blocktype = $matches['blocktype'];
            $this->blocknumber = $matches['blocknumber'];
        }

        $this->rownumber = $matches['rownumber'];

        $this->alignment = null;
        if (isset($matches['alignment'])) {
            $this->alignment = $matches['alignment'];
        }
    }

    public function SetText($text)
    {
        $this->text = $text;
    }

    /* Syntax of the location:
     *     <page><block><row>
     * where
     *     <page> ::= <folionumber><sidetype>
     *          <folionumber> is an integer on three digits
     *          <sidetype> ∈ { r, v } -- r[ecto], v[erso]
     *     <block> ::= <blocktype><blocknumber>:
     *          <blocktype> ∈ { b, c, p, h } -- b[lock], c[olumn], p[icture], h[eader]
     *          <blocknumber> is '' in the case of blocktype 'h', integer on one digit in the rest of the cases
     *     <row> ::= <rownumber>[<alignment>]
     *          <rownumber> is an integer on two digits
     *          <alignment> ∈ { c, s, e } -- c[enter], s[tart], e[nd]
     * If no block is specified, row is a part of the "main text" which runs through all pages.
     * Blocks of type 'column' (c) are part of the main text. Other types of blocks are individual text units.
     */
    public static function ValidateLocation($location)
    {
        $result = preg_match("/^(?P<folio>\d\d\d)(?P<side>[rv])((?P<header>h)|((?P<blocktype>[bcp])(?P<blocknumber>\d):))?(?P<rownumber>\d\d)(?P<alignment>[cse])?$/", $location, $matches);
        if ($result === false) {
            throw new \Exception("Error in preg_match: ValidateLocation($location)");
        }
        return [
            'isValid' => ($result === 1),
            'matches' => $matches,
        ];
    }
}
