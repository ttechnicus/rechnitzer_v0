<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\DictionaryReader;
use App\Dictionary;

class ParseDictionary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:dictionary {filename} {language} {--insert} {--verboseMode}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses the source file to validate and load as a dictionary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dictionaryReader = new DictionaryReader($this->argument('filename'));

        try {
            $dictionary = $dictionaryReader->Parse($this->IsVerboseMode());
        } catch (\Exception $e) {
            print $e->getMessage() . "\n";
            die();
        }

        if ($this->option('insert') === true) {
            if ($this->IsVerboseMode()) {
                print "Starting inserting into database...\n";
            }
            foreach ($dictionary as $entry) {
                if ($this->IsVerboseMode()) { print $entry['code'] . ' '; }
                $this->CreateDictionaryEntry($entry['code'], $entry['row'], $this->argument('language'));
            }
            if ($this->IsVerboseMode()) {
                print "\nFinished inserting into database.\n";
            }
        }
    }

    private function IsVerboseMode()
    {
        return $this->option('verboseMode') === true;
    }

    private function CreateDictionaryEntry($code, $text, $language)
    {
        $entry = new Dictionary;

        $entry->code = $code;
        $entry->entry = $text;
        $entry->language = $language;

        $entry->save();
    }
}
