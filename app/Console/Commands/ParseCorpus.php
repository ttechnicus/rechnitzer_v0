<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\CorpusReader;
use App\Corpus;

class ParseCorpus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:corpus {filename} {--insert}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses the source file to vaildate and load as the corpus';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        print "Opening source file...\n";
        $corpusReader = new CorpusReader($this->argument('filename'));
        print "Opening done.\n";

        print "Parsing source file...\n";
        try {
            $corpus = $corpusReader->Parse();
        } catch (\Exception $e) {
            print $e->getMessage() . "\n";
            die();
        }
        print "Parsing done.\n";

        if ($this->option('insert') === true) {
            print "Saving corpus into database...\n";
            $currentPage = '';
            foreach ($corpus as $row) {
                $currentLocationPage = mb_substr($row['location'], 0, 4);
                if ($currentPage != $currentLocationPage) {
                    print $currentLocationPage . ' ';
                    $currentPage = $currentLocationPage;
                }
                $this->CreateCorpusRow($row['location'], $row['text']);
            }
            print "\nSaving done.\n";
        }
    }

    private function CreateCorpusRow($location, $text)
    {
        $corpus = new Corpus;

        $corpus->SetLocation($location);
        $corpus->SetText($text);

        $corpus->save();
    }
}
