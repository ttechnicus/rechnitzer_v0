<?php

namespace App\Helpers;

class ConverterHelper
{
    static function Convert($text, $toVersion, $fromVersion) {
        if (!self::IsValidConversion($toVersion, $fromVersion)) {
            return null;
        }

        $dictionary = self::GetSymbolDictionary($toVersion, $fromVersion);

        $translation = "";
        $debug = [];
        $untranslated = [];
        for ($i = 0; $i < mb_strlen($text); $i++) {
            $from = sprintf("%04s", strtoupper(dechex(mb_ord(mb_substr($text, $i, 1)))));
            $to = $from;
            if (isset($dictionary[$from])) {
                $to = $dictionary[$from];
            } else {
                $untranslated[] = $from;
            }
            $debug[] = [$from, $to];
            $translation .= mb_chr(hexdec($to));
        }

        return [ 'debug' => $debug, 'translation' => $translation, 'untranslated' => $untranslated ];
    }

    private static function IsValidConversion($toVersion, $fromVersion) {
        return ($toVersion != 'v2_0' && $fromVersion != 'v1_404')
            || ($toVersion != 'v1_404' && $fromVersion != 'v2_0');
    }

    private static function GetSymbolDictionary($toVersion, $fromVersion) {
        $dictionary = [];
        foreach (\App\Symbol::select($fromVersion, $toVersion)->get()->toArray() as $pair) {
            $dictionary[$pair[$fromVersion]] = $pair[$toVersion];
        }
        return $dictionary;
    }
}
