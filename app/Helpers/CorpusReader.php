<?php

namespace App\Helpers;

use App\Corpus;

class CorpusReader
{
    private $filerows = [];
    private $actualRow;
    private $actualRowNumber;
    private $actualLocation;
    private $actualPageNumber;
    private $actualText;

    private $regexGetParts = "/^(?P<location>[^\s]+)\s(?P<text>[^#]+)(\s#\s*)?$/u";
    private $regexIsEmpty = "/^\x{FEFF}?$/u";
    private $regexIsCommentedOut = "/^\x{FEFF}?#/u";

    public function __construct($filename)
    {
        $this->filerows = file($filename);
    }

    public function Parse()
    {
        $codexRows = [];

        foreach ($this->filerows as $rownumber => $row) {
            $this->actualRow = trim($row);
            $this->actualRowNumber = $rownumber;

            if ($this->IsActualRowCommentedOut() || $this->IsActualRowEmpty()) {
                continue;
            }

            $this->actualLocation = $this->GetLocation();

            try {
                $validation = Corpus::ValidateLocation($this->actualLocation);
                if (!$validation['isValid']) {
                    $this->PrintInvalidMessage();
                    continue;
                }
            } catch (\Exception $e) {
                throw new \Exception("Unexpected parse error in row " . $this->actualRowNumber . ": " . $e->getMessage());
            }

            if ($this->actualPageNumber != mb_substr($this->actualLocation, 0, 4)) {
                $this->actualPageNumber = mb_substr($this->actualLocation, 0, 4);
                $this->LogLocation();
            }
            $this->actualText = $this->GetText();
            $this->actualText = $this->ConvertRohoncMirroredTextToRohoncCodex($this->actualText);

            $codexRows[] = [
                'location' => $this->actualLocation,
                'text' => $this->actualText,
            ];
        }

        print "\n";

        return $codexRows;
    }

    private function PrintInvalidMessage()
    {
        fwrite (STDERR, "\n"
            . "Invalid row in line " . $this->actualRowNumber . "\n"
            . "text: " . $this->actualRow
            . "\n");
    }

    private function IsActualRowCommentedOut()
    {
        return preg_match($this->regexIsCommentedOut, $this->actualRow);
    }

    private function IsActualRowEmpty()
    {
        return preg_match($this->regexIsEmpty, $this->actualRow);
    }

    private function GetLocation()
    {
        preg_match($this->regexGetParts, $this->actualRow, $matches);
        $location = $matches['location'];
        return $location;
    }

    private function GetText()
    {
        preg_match($this->regexGetParts, $this->actualRow, $matches);
        $text = $matches['text'];
        return $text;
    }

    private function ConvertRohoncMirroredTextToRohoncCodex()
    {
        $translation = ConverterHelper::Convert($this->actualText, 'v2_0', 'v1_404');
        if (count($translation['untranslated'])) {
            fwrite(STDERR, "\n"
                . "Warning: untranslated code chunks in row "
                . $this->actualRowNumber
                . " (location "
                . $this->actualLocation
                . ") '"
                . $this->actualText
                . "': "
                . implode(', ', $translation['untranslated'])
                . "\n"
            );
        }
        return $translation['translation'];
    }

    private function LogLocation()
    {
        print $this->actualPageNumber . ' ';
    }
}
