<?php

namespace App\Helpers;

class DictionaryReader
{
    private $filerows = [];
    private $actualRow;
    private $actualRowNumber;
    private $actualCode;

    private $regexValidRow                    = "/^(?P<numbering>\x{FEFF}?\s*\d+\.\s[^\s]+\s)\x{2006}(?P<code>[^\x{2006}\x{2007}]*)\x{2007}(?P<homonr>\s\(\d\))?(?:[,;]\s(?P<uncertainty>\x{2000}\?+\x{2001}\s)?(?P<altcode>\x{2006}[^\x{2006}\x{2007}]*\x{2007}))*\s(?P<notarrow>[^\x{2192}]{2})/u";
    private $regexReferenceRow                = "/^(?P<numbering>\x{FEFF}?\s*\d+\.\s[^\s]+\s)\x{2006}(?P<code>[^\x{2006}\x{2007}]*)\x{2007}(?P<homonr>\s\(\d\))?(?:[,;]\s(?P<uncertainty>\x{2000}\?+\x{2001}\s)?(?P<altcode>\x{2006}[^\x{2006}\x{2007}]*\x{2007}))*\s(?P<arrowmarkup>\x{2006})?(?P<arrow>\x{2192})/u";
    private $regexStripCodeMarkup             = "/[\x{2053}\x{2056}\x{2004}\x{2005}]/u";
    private $regexStripNumberingFromBeginning = "/^(?P<numbering>\x{FEFF}?\s*\d+\.\s[^\s]+\s)/u";
    private $regexRohoncWithMarkup            =                                            "/\x{2006}(?P<code>[^\x{2006}\x{2007}]*)\x{2007}/u";
    private $regexRohoncMarkupCharacters      = "/(?P<markupcharacter>[\x{2000}\x{2001}\x{2002}\x{2003}\x{2004}\x{2005}\x{2006}\x{2007}\x{2050}\x{2051}])/u";
    private $regexGetConvertedCode            =                               "/^[^\x{F000}]*\x{F000}(?P<code>[^\x{F000}\x{F001}]*)\x{F001}/u";

    public function __construct($filename)
    {
        $this->filerows = file($filename);
    }

    public function Parse($isVerbose)
    {
        $dictionary = [];

        if ($isVerbose) {
            $this->log("Starting parsing input file... ("
                . count($this->filerows)
                . " rows)\nRow");
        }

        foreach ($this->filerows as $rownumber => $row) {
            $this->actualRow = trim($row);
            $this->actualRowNumber = $rownumber;

            if ($isVerbose) { $this->log(' ' . $rownumber); }

            try {
                if (!$this->IsActualRowValid()) {
                    if (!$this->IsActualRowReference()) {
                        $this->PrintInvalidMessage();
                    }
                    continue;
                }
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            $this->actualRow = $this->ConvertRohoncMirroredToRohoncCodex($this->actualRow);
            $this->actualCode = $this->GetCode($this->actualRow);
            $this->actualRow = $this->StripNumberingFromBeginning($this->actualRow);

            $dictionary[] = [
                'code' => $this->actualCode,
                'row' => $this->actualRow,
            ];
        }

        if ($isVerbose) {
            $this->log("\nFinished parsing input file.\n");
        }

        return $dictionary;
    }

    private function PrintInvalidMessage()
    {
        $this->log("\n"
            . "Invalid row in line " . $this->actualRowNumber . "\n"
            . "text: " . $this->actualRow
            . "\n");
    }

    private function IsActualRowValid()
    {
        return preg_match($this->regexValidRow, $this->actualRow);
    }

    private function IsActualRowReference()
    {
        return preg_match($this->regexReferenceRow, $this->actualRow);
    }

    private function GetCode($row)
    {
        preg_match($this->regexGetConvertedCode, $row, $matches);
        $code = $matches['code'];
        $code = preg_replace($this->regexStripCodeMarkup, '', $code);
        return $code;
    }

    private function StripNumberingFromBeginning($row)
    {
        return preg_replace($this->regexStripNumberingFromBeginning, '', $row);
    }

    private function ConvertRohoncMirroredToRohoncCodex($row)
    {
        $row = $this->ConvertRohoncChunks($row);
        $row = $this->ConvertRohoncMarkupCharacters($row);
        return $row;
    }

    private function ConvertRohoncChunks($row)
    {
        return preg_replace_callback($this->regexRohoncWithMarkup, function($matches) {
            $translation = ConverterHelper::Convert($matches['code'], 'v2_0', 'v1_404');
            if (count($translation['untranslated'])) {
                $this->log("\n"
                    . "Warning: untranslated code chunks in code "
                    . $matches[0] . ": \Ux"
                    . implode(', \Ux', $translation['untranslated'])
                    . "\n"
                );
            }
            $prefix = mb_substr($matches[0], 0, 1);
            $postfix = mb_substr($matches[0], -1, 1);
            return $prefix . $translation['translation'] . $postfix;
        }, $row);
    }

    private function ConvertRohoncMarkupCharacters($row)
    {
        return preg_replace_callback($this->regexRohoncMarkupCharacters, function($matches) {
            $translation = ConverterHelper::Convert($matches['markupcharacter'], 'v2_0', 'v1_404');
            if (count($translation['untranslated'])) {
                $this->log("\n"
                    . "Warning: untranslated markup character: \Ux"
                    . implode(', \Ux', $translation['untranslated'])
                    . "\n"
                );
            }
            return $translation['translation'];
        }, $row);
    }

    private function log($message)
    {
        fwrite(STDERR, $message);
    }
}
