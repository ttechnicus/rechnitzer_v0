<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Corpus;

class CorpusTest extends TestCase
{
    /**
     * Test validating a malformed location
     * Missing folio number
     *
     * @return void
     */
    public function testValidateLocation_MissingFolioNumber_IsNotValid()
    {
        $location = "v01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a malformed location
     * Malformed folio number
     *
     * @return void
     */
    public function testValidateLocation_MalformedFolioNumber_IsNotValid()
    {
        $location = "4v01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a malformed location
     * Missing side
     *
     * @return void
     */
    public function testValidateLocation_MissingSide_IsNotValid()
    {
        $location = "00401";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a malformed location
     * Nonexistent side
     *
     * @return void
     */
    public function testValidateLocation_NonexistentSide_IsNotValid()
    {
        $location = "004c01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a malformed location
     * No row number
     *
     * @return void
     */
    public function testValidateLocation_NoRowNumber_IsNotValid()
    {
        $location = "004v";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a malformed location
     * Malformed row number
     *
     * @return void
     */
    public function testValidateLocation_MalformedRowNumber_IsNotValid()
    {
        $location = "004v003";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a correct Haupttext location
     *
     * @return void
     */
    public function testValidateLocation_HauptTextLocation_IsValid()
    {
        $location = "004v01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertTrue($validation['isValid']);
    }

    /**
     * Test validating a malformed Header location
     *
     * @return void
     */
    public function testValidateLocation_MalformedHeaderLocation_IsNotValid()
    {
        $location = "004vh:01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a correct Header location
     *
     * @return void
     */
    public function testValidateLocation_HeaderLocation_IsValid()
    {
        $location = "004vh01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertTrue($validation['isValid']);
    }

    /**
     * Test validating a malformed TextBlock location
     *
     * @return void
     */
    public function testValidateLocation_MalformedTextBlockLocation_IsNotValid()
    {
        $location = "004vb01:01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a malformed TextBlock location having no blocknumber
     *
     * @return void
     */
    public function testValidateLocation_MalformedTextBlockLocationNoBlocknumber_IsNotValid()
    {
        $location = "004vb:01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a correct TextBlock location
     *
     * @return void
     */
    public function testValidateLocation_TextBlockLocation_IsValid()
    {
        $location = "004vb1:01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertTrue($validation['isValid']);
    }

    /**
     * Test validating a malformed Column location
     *
     * @return void
     */
    public function testValidateLocation_MalformedColumn_IsNotValid()
    {
        $location = "004vc:01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a correct Column location
     *
     * @return void
     */
    public function testValidateLocation_Column_IsValid()
    {
        $location = "004vc2:01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertTrue($validation['isValid']);
    }

    /**
     * Test validating a malformed PictureText location
     *
     * @return void
     */
    public function testValidateLocation_MalformedPictureTextLocation_IsNotValid()
    {
        $location = "004vp2:001";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a correct PictureText location
     *
     * @return void
     */
    public function testValidateLocation_PictureTextLocation_IsValid()
    {
        $location = "004vp2:01";
        $validation = Corpus::ValidateLocation($location);
        $this->assertTrue($validation['isValid']);
    }

    /**
     * Test validating a location with an incorrect (nonexistent) alignment
     *
     * @return void
     */
    public function testValidateLocation_LocationWithIncorrectAlignment_IsNotValid()
    {
        $location = "004v01f";
        $validation = Corpus::ValidateLocation($location);
        $this->assertFalse($validation['isValid']);
    }

    /**
     * Test validating a correct BlockText location with centered alignment
     *
     * @return void
     */
    public function testValidateLocation_BlockTextLocationWithCenteredAlignment_IsValid()
    {
        $location = "004vb2:01c";
        $validation = Corpus::ValidateLocation($location);
        $this->assertTrue($validation['isValid']);
    }

    /**
     * Test validating a correct BlockText location with start-alignment
     *
     * @return void
     */
    public function testValidateLocation_BlockTextLocationWithStartAlignment_IsValid()
    {
        $location = "004vb2:01s";
        $validation = Corpus::ValidateLocation($location);
        $this->assertTrue($validation['isValid']);
    }

    /**
     * Test validating a correct BlockText location with end-alignment
     *
     * @return void
     */
    public function testValidateLocation_BlockTextLocationWithEndAlignment_IsValid()
    {
        $location = "004vb2:01e";
        $validation = Corpus::ValidateLocation($location);
        $this->assertTrue($validation['isValid']);
    }
}

