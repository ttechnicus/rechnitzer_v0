<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corpora', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedSmallInteger('folio')->nullable(false);
            $table->char('side', 1)->nullable(false);
            $table->char('blocktype', 1)->nullable(true);
            $table->unsignedSmallInteger('blocknumber')->nullable(true);
            $table->unsignedSmallInteger('rownumber')->nullable(false);
            $table->char('alignment', 1)->nullable(true);
            $table->string('text', 64);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corpora');
    }
}
