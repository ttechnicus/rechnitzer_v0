<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSymbolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('symbols', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('k_v5', 4)->nullable(true);
            $table->char('v1_404', 4);
            $table->string('shortcut', 16)->nullable(true);
            $table->string('pre', 8)->nullable(true);
            $table->string('post', 8)->nullable(true);
            $table->boolean('nontext')->nullable(true);
            $table->char('v2_0', 4);
            $table->string('type', 8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('symbols');
    }
}
