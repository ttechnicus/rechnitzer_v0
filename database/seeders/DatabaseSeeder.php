<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // run *.sql files found in the current directory
        foreach (glob("database/seeds/*.sql") as $filename) {
            $sqlCommand = file_get_contents($filename);
            print "seeding from file: $filename...\n";
            DB::insert($sqlCommand);
        }
    }
}
