<?php

use Illuminate\Http\Request;
use App\Http\Controllers\ConverterController;
use App\Http\Controllers\DictionaryController;
use App\Http\Controllers\PageViewController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
/**
 * Converter
 */
Route::post('/Convert', [ConverterController::class, 'Convert']);
Route::get('/GetConversionTable', [ConverterController::class, 'GetConversionTable']);

/**
 * Dictionary
 */
Route::get('/GetDictionary/{language}', [DictionaryController::class, 'GetDictionary']);

/**
 * PageViewer
 */
Route::get('/GetPageList', [PageViewController::class, 'GetPageList']);
Route::get('/GetPage/{page}', [PageViewController::class, 'GetPage']);
