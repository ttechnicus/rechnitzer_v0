/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';

window.Vue = Vue;


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
import InfoModal from './components/InfoModal.vue';
import Converter from './components/Converter.vue';
import DictionaryBrowser from './components/DictionaryBrowser.vue';
import PageViewer from './components/PageViewer.vue';
import TextBlock from './components/TextBlock.vue';

Vue.component('info-modal', InfoModal);
Vue.component('converter', Converter);
Vue.component('dictionary-browser', DictionaryBrowser);
Vue.component('page-viewer', PageViewer);
Vue.component('text-block', TextBlock);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
