<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Rohonc Font converter</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <style>
            html, body {
                background-color: #f8fafc;
                color: #212529;
                height: 100vh;
                margin: 0 10px;
            }

            #logo {
                font-size: 42px;
                padding: 10px 10px 10px 0;
            }
        </style>

        <!-- Starting the Vue engine -->
        <script src="{{ asset('js/app.js') }}" defer></script>

    </head>
    <body>
        <header id="header">
            <div id="logo">
                Rohonc Codex font converter
            </div>
        </header>
        <div id="app">
            <converter></converter>
        </div>
    </body>
</html>
