<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Der Rechnitzer Kodex</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Volkhov:700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <style>
            html, body {
                height: 100vh;
                background-color: #f8fafc;
                color: #212529;
                font-family: 'Open Sans', sans-serif;
                font-weight: 400;
                margin: 0;
            }
            #app {
                height: 100vh;
                display: flex;
                flex-flow: column;
            }

            #header {
                flex: 0 1 auto;
                display: flex;
                flex-flow: row nowrap;
                justify-content: space-between;
                align-items: center;
                padding: .5rem;
            }
            #logo {
                font-family: 'Volkhov', serif;
                font-weight: 700;
                font-size: 42px;
            }

            main {
                flex: 1 1 auto;
                overflow-y: auto;
                display: flex;
                flex-flow: column;
            }
            main>* {
                flex: 1;
            }
            .flex2 {
                flex: 2;
            }

            @media only screen and (min-width: 960px) {
                main {
                    flex-flow: row nowrap;
                }
            }
        </style>

        <!-- Starting the Vue engine -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body>
        <div id="app">
            <header id="header">
                <div id="logo">Der Rechnitzer Kodex</div>
                <info-modal></info-modal>
            </header>
            <main>
                <page-viewer class="flex2"></page-viewer>
                <dictionary-browser></dictionary-browser>
            </main>
        </div>
    </body>
</html>
